//#pragma once
#include <cstdio>
#include <cstring>
#include <iostream>
#include "tool.h"
//#include "map.h"
#include "map.hpp"
namespace wyll
{
	double price_to_double(char* s)
	{
		int i = 0;
		while (s[i] > '9' || s[i] < '0')i++;
		double ans = 0;
		while (s[i] != '.')
		{
			ans = ans * 10 + (s[i] - '0');
			i++;
		}
		while (s[i] != '\0')i++;
		i--;
		double dot = 0;
		while (s[i] != '.')
		{
			dot = dot * 0.1 + (s[i] - '0');
			i--;
		}
		dot *= 0.1;
		ans += dot;
		return ans;
	}
	class string
	{
	public:
		char storage[50];
		size_t len;
		string():len(){}
		string(const char* s)
		{
			len = strlen(s);
			strncpy(storage, s, len);
		}
		string& operator=(const string& other)
		{
			len = other.len;
			strncpy(storage, other.storage, len);
			return *this;
		}
		string(const string& other)
		{
			len = other.len;
			strncpy(storage, other.storage, len);
		}
		~string()
		{
			len = 0;
		}
		bool operator==(const string& other)const
		{
			if (len != other.len) return false;
			bool flag = strncmp(other.storage, storage, len);
			if (!flag) return true;
			else return false;
		}
		bool operator!= (const string& other)const
		{
			return !(*this == other);
		}
		bool operator<(const string& other)const
		{
			int del = strncmp(storage, other.storage, len);
			if (del < 0)return true;
			else return false;
		}
		friend std::ostream& operator<<(std::ostream &output, const string& out)
		{
			if (out.storage != NULL)
				output << out.storage;
			return output;
		}
		friend std::istream& operator>>(std::istream& input, string& in)
		{
			char s[50];
			input >> s;
			in = s;
			return input;
		}
	};
	int string_to_date(string s)
	{
		int l = s.len;
		int ans = (s.storage[l - 2] - '0') * 10;
		ans += s.storage[l - 1] - '0';
		return ans;
	}
	class time
	{
	public:
		string text;
		int hour, minute;
		time() :text() { hour = 0; minute = 0; }
		time(char*s) :text(s)//保证s合法
		{
			//会有xx:xx这种数据
			char c1 = text.storage[0];
			if (c1 >= '0' && c1 <= '9')
			{
				hour = (text.storage[0] - '0') * 10 + (text.storage[1] - '0');
				minute = (text.storage[3] - '0') * 10 + (text.storage[4] - '0');
			}
			else
			{
				hour = 0;
				minute = 0;
			}
		}
		time(const time& other) :text(other.text)
		{
			hour = other.hour;
			minute = other.minute;
		}
		time& operator=(const time& other)
		{
			if (this == &other)return *this;
			text = other.text;
			hour = other.hour;
			minute = other.minute;
			return *this;
		}
		int operator- (const time& other)
		{
			int ans = (hour - other.hour) * 60 + minute - other.minute;
			return ans;
		}
		friend std::ostream& operator<< (std::ostream& out, const time& t)
		{
			out << t.text;
			return out;
		}
		friend std::istream& operator>> (std::istream& in, time& t)
		{
			char s[50];
			in >> s;
			t = time(s);
			return in;
		}
	};
	class ticket
	{
	public:
		string train_id;
		string loc_from, loc_to;
		char catalog;
		int date_from, date_to;
		time t_from, t_to;
		bool operator==(const ticket& other)
		{
			if (train_id != other.train_id)return false;
			if (loc_from != other.loc_from)return false;
			if (loc_to != other.loc_to)return false;
			if (date_from != other.date_from)return false;
			return true;
		}
		int num;//座位种类
		class data
		{
		public:
			string ticket_kind;
			int left;//剩余或拥有座位数
			double price;
			data() :ticket_kind() {}
			data(string s, int l, double p) :ticket_kind(s), left(l), price(p) {}
			data& operator=(const data& other)
			{
				if (this == &other)return *this;
				ticket_kind = other.ticket_kind;
				left = other.left;
				price = other.price;
				return *this;
			}
		};
		data seat[15];
		friend std::ostream& operator<< (std::ostream& out, const ticket& tic)
		{
			out << tic.train_id << ' ';
			out << tic.loc_from << ' ' << "2018-06-" << tic.date_from << ' ' << tic.t_from << ' ';
			out << tic.loc_to << ' ' << "2018-06-" << tic.date_to << ' ' << tic.t_to;
			for (int i = 0; i < tic.num; i++)
			{
				out << ' ' << tic.seat[i].ticket_kind << ' ' << tic.seat[i].left << ' ' << tic.seat[i].price;
			}
			return out;
		}
	};

	class user
	{
	public:
		static size_t num;
//#define max_ticket 100
		//static const int max_ticket;
		static map<0, size_t, user> u_store;
		static map<1, size_t, ticket> tic_store;
		string name, password, email, phone;
		size_t id;
		bool privilege;
		//ticket possess[max_ticket];//不同date的ticket不可合并
		int ticket_num;
		user() {}
		user(string n, string p, string e, string ph)
		{
			name = n;
			password = p;
			email = e;
			phone = ph;

			id = num + 2018;
			if (id == 2018)privilege = true;
			else privilege = false;
		}
		static size_t newuser(string n, string p, string e, string ph)
		{
			user tmp(n, p, e, ph);
			++num;
			u_store.insert(tmp.id, tmp);
			return tmp.id;
		}
		
		user(const user& other)
		{
			name = other.name;
			password = other.password;
			email = other.email;
			phone = other.phone;
			id = other.id;
			privilege = other.privilege;
		}
		user& operator=(const user& other)
		{
			if (this == &other)return *this;
			name = other.name;
			password = other.password;
			email = other.email;
			phone = other.phone;
			id = other.id;
			privilege = other.privilege;
			return *this;
		}
		~user()
		{
		}
		bool up_privilege(size_t id2)
		{
			if (privilege == false) return false;
			user tmp = u_store[id2];
			if (tmp.privilege)return true;
			tmp.privilege = true;
			u_store.insert(id2, tmp);
			return true;
		}
		bool login(const string& input)const
		{
			if (password == input)return true;
			else return false;
		}
		void modify(string n, string p, string e, string ph)
		{
			name = n;
			password = p;
			email = e;
			phone = ph;
		}
		void enquiry()
		{
			std::cout << name << ' ' << email << ' ' << phone << '\n';
		}
	};
	size_t user::num = 0;
	map<0, size_t, user> user::u_store("user.txt");
	map<1, size_t, ticket> user::tic_store("ticket.txt");
	//const int user::max_ticket = 100;
	
	class station
	{
	public:
		string name;
		time arrive, start, stopover;
		int num_price;
		double price[15];
		int sale[15][40];//sale[seat][date]表示seat座位第date天剩余的票数
		friend std::ostream& operator<< (std::ostream& out, const station& s)
		{
			out << s.name << ' ' << s.arrive << ' ' << s.start << ' ' << s.stopover;
			for (int i = 0; i < s.num_price; i++)
				out << ' ' << s.price[i];
			return out;
		}
	};
	
	class train
	{
	public:
		static map<0, string, train> tr_store; 
		static map<1, string, string> world_map;//loc-train_id 的一对多映射
		static vector<station> passes;//所有train的station信息

		string train_id, name;
		char catalog;
		bool sale;
		int num_station, num_price;
		string seat[15];//每一种seat的名字
		
		int pass_start, pass_end;//passes[pass_start, pass_end]存这个train的所有station
	//	station pass[70];
		train(){}
		train(string tr_id, string n, char c, int n_s, int n_p):train_id(tr_id), name(n), catalog(c), num_station(n_s), num_price(n_p)
		{
			sale = false;
		}
		void add_station_and_seat()//这函数得改。。
		{
			for (int i = 0; i < num_price; i++)
				std::cin >> seat[i];
			station tmp;
			for (int i = 0; i < num_station; i++)
			{
				std::cin >> tmp.name >> tmp.arrive >> tmp.start >> tmp.stopover;
				char s[100];
				for (int j = 0; j < num_price; j++)
				{
					std::cin >> s;
					tmp.price[j] = price_to_double(s);
				}
				tmp.num_price = num_price;
				//初始化剩余票数
				for (int j = 0; j < num_price; j++)
					for (int k = 1; k <= 30; k++)
					{
						tmp.sale[j][k] = 2000;
					}
				//塞到vector里
				if (i == 1)
					pass_start = passes.pushback(tmp);
				else pass_end = passes.pushback(tmp);
			}
		}
		static void new_train(string tr_id, string n, char c, int n_s, int n_p)
		{
			train tmp(tr_id, n, c, n_s, n_p);
			tmp.add_station_and_seat();
			tr_store.insert(tr_id, tmp);
		}
		void query()//其实就是一个<<重载
		{
			std::cout << train_id << ' ' << name << ' ' << catalog << ' ' << num_station << ' ' << num_price;
			for (int i = 0; i < num_price; i++)
				std::cout << ' ' << seat[i];
			std::cout << '\n';
			for (int i = 0; i < num_station; i++)
				std::cout << passes[i + pass_start] << '\n';
		}
		bool del()
		{
			if (sale == true)return false;
			tr_store.erase(train_id);
			return true;
		}
		void modify(string n, char c, int n_s, int n_p)
		{
			name = n;
			catalog = c;
			num_station = n_s;
			num_price = n_p;
			add_station_and_seat();
		}
		bool sale_train()
		{
			if (sale == true)return false;
			string s;
			for (int i = 0; i < num_station; i++)
			{
				s = passes[i + pass_start].name;
				world_map.insert(s, train_id);
			}
			sale = true;
			return true;
		}
	};
	map<0, string, train> train::tr_store("train.txt");
	map<1, string, string> train::world_map("world_map.txt");
	vector<station> train::passes("passes.txt");
	

	/*
	=====================================================
	===下面是各个command的函数实现=======================
	=====================================================
	*/

	void register_()
	{
		char name[200], password[200], email[200], phone[200];
		std::cin >> name >> password >> email >> phone;
		size_t id = user::newuser(name, password, email, phone);
		std::cout << id << '\n';
	}
	void login()
	{
		size_t id;
		char password[200];
		std::cin >> id >> password;
		if (!user::u_store.exist(id))
		{
			std::cout << 0 << '\n';
			return;
		}
		string pass(password);
		user tmp = user::u_store[id];
		bool flag = tmp.login(pass);
		std::cout << flag << '\n';
	}
	void query_profile()
	{
		size_t id;
		std::cin >> id;
		if (!user::u_store.exist(id))
		{
			std::cout << 0 << '\n';
			return;
		}
		user tmp = user::u_store[id];
		tmp.enquiry();
		return;
	}
	void modify_profile()
	{
		size_t id;
		char name[200], password[200], email[200], phone[200];
		std::cin >> id >> name >> password >> email >> phone;
		if (!user::u_store.exist(id))
		{
			std::cout << 0 << '\n';
			return;
		}
		user tmp(user::u_store[id]);
		tmp.name = name;
		tmp.password = password;
		tmp.email = email;
		tmp.phone = phone;
		user::u_store.insert(id, tmp);//修改
		std::cout << 1 << '\n';
		return;
	}
	void modify_privilege()
	{
		size_t id1, id2;
		int privilege;
		std::cin >> id1 >> id2 >> privilege;
		if (privilege == 0)
		{
			std::cout << 0 << '\n';
			return;
		}
		user tmp = user::u_store[id1];
		bool flag = tmp.up_privilege(id2);
		std::cout << flag << '\n';
	}
	void query_order()
	{
		size_t id;
		int date;
		char catalog;
		std::cin >> id >> date >> catalog;
		map<1, size_t, ticket>::iterator start, end, tmp;
		start = user::tic_store.lowerbound(id);
		end = user::tic_store.upperbound(id);
		//user tmp = user::u_store[id];
		ticket t;
		for (tmp = start; tmp != end; tmp++)
		{
			t = *tmp;
			if (t.date_from == date && t.catalog == catalog)
				std::cout << t << '\n';
		}
	}
	void add_train()
	{
		string tr_id, name;
		char catalog;
		int n_s, n_p;
		std::cin >> tr_id >> name >> catalog >> n_s >> n_p;
		train::new_train(tr_id, name, catalog, n_s, n_p);
		std::cout << '1' << '\n';
	}
	void sale_train()
	{
		string tr_id;
		std::cin >> tr_id;
		train tmp = train::tr_store[tr_id];
		bool flag = tmp.sale_train();
		if (!flag)std::cout << 0 << '\n';
		else
		{
			train::tr_store.insert(tr_id, tmp);
			std::cout << 1 << '\n';
		}
	}
	void query_train()
	{
		string tr_id;
		std::cin >> tr_id;
		train tmp = train::tr_store[tr_id];
		tmp.query();
	}
	void delete_train()
	{
		string tr_id;
		std::cin >> tr_id;
		if (!train::tr_store.exist(tr_id))
		{
			std::cout << 0 << '\n';
			return;
		}
		train tmp = train::tr_store[tr_id];
		if (tmp.sale)
		{
			std::cout << 0 << '\n';
			return;
		}
		train::tr_store.erase(tr_id);
		std::cout << 1 << '\n';
	}
	void modify_train()
	{
		string tr_id, name;
		char c;
		int n_s, n_p;
		std::cin >> tr_id >> name >> c >> n_s >> n_p;
		train tmp1(tr_id, name, c, n_s, n_p);
		tmp1.add_station_and_seat();
		if (!train::tr_store.exist(tr_id))
		{
			std::cout << 0 << '\n';
			return;
		}
		train tmp = train::tr_store[tr_id];
		if (tmp.sale)
		{
			std::cout << 0 << '\n';
			return;
		}
		train::tr_store.insert(tr_id, tmp);
		std::cout << 1 << '\n';
	}
	void buy_ticket()
	{
		string tr_id, loc1, loc2, ticket_kind, odate;
		int num, date, id;
		std::cin >> id >> num >> tr_id >> loc1 >> loc2 >> odate >> ticket_kind;
		date = string_to_date(odate);
		train tr_tmp = train::tr_store[tr_id];
		int start = tr_tmp.pass_start, end = tr_tmp.pass_end;
		while (train::passes[start].name != loc1)start++;
		while (train::passes[end].name != loc2)end--;
		int seat = 0;
		for (; seat < tr_tmp.num_price; seat++)
		{
			if (tr_tmp.seat[seat] == ticket_kind)break;
		}
		//没有找到相应的座位
		if (seat == tr_tmp.num_price)
		{
			std::cout << 0 << '\n';
			return;
		}
		//票数不够
		for (int i = start; i <= end; i++)
		{
			if (train::passes[i].sale[seat][date] < num)
			{
				std::cout << 0 << '\n';
				return;
			}
		}
		//火车上减票数
		int duration = 0;//跨的天数
		station tmp;
		for (int i = start; i <= end; i++)
		{
			tmp = train::passes[i];
			tmp.sale[seat][date] -= num;
			if (i != end && tmp.start - tmp.arrive >= 0)duration++;
				//前一站时间比后一站时间大，说明跨了一天。
			//tmp 塞回去
			train::passes.edit(i, tmp);
		}
		train::tr_store.insert(tr_id, tr_tmp);
		//创建这个ticket对象
		ticket tic_tmp;
		tic_tmp.date_from = date;
		tic_tmp.date_to = date + duration;
		tic_tmp.loc_from = loc1;
		tic_tmp.loc_to = loc2;
		tic_tmp.t_from = train::passes[start].start;
		tic_tmp.t_to = train::passes[end].arrive;
		tic_tmp.train_id = tr_id;
		tic_tmp.catalog = tr_tmp.catalog;

		//看看用户已有车票里边有没有同类型的
		map<1, size_t, ticket>::iterator l_it, u_it, t_it;
		l_it = user::tic_store.lowerbound(id);
		u_it = user::tic_store.upperbound(id);
		bool flag = false;
		for (t_it = l_it; t_it != u_it; t_it++)
		{
			if (*t_it == tic_tmp)
			{
				ticket t = *t_it;
				t.seat[seat].left += num;
				//undo::修改map中这个ticket的信息
				flag = true;
			}
		}
		if (!flag)//没有相同类型的，即新建
		{
			//我买一种票，要把所有的座位种类都copy过来。。。
			tic_tmp.num = tr_tmp.num_price;
			for (int i = 0; i < tic_tmp.num; i++)
			{
				tic_tmp.seat[i].ticket_kind = tr_tmp.seat[i];
				tic_tmp.seat[i].left = 0;
			}
			//买票
			tic_tmp.seat[seat].left = num;
			user::tic_store.insert(id, tic_tmp);
			user u_tmp = user::u_store[id];
			u_tmp.ticket_num++;
			user::u_store.insert(id, u_tmp);
		}
		return;
	}
	void refund_ticket()
	{
		size_t id;
		int num, date;
		string tr_id, loc1, loc2, odate, kind;
		std::cin >> id >> num >> tr_id >> loc1 >> loc2 >> odate >> kind;
		date = string_to_date(odate);
		ticket t_tmp;
		t_tmp.train_id = tr_id;
		t_tmp.loc_from = loc1;
		t_tmp.loc_to = loc2;
		t_tmp.date_from = date;
		user u_tmp = user::u_store[id];
		bool flag = false;
		int seat = 0;
		map<1, size_t, ticket>::iterator u_it, l_it, t_it;
		l_it = user::tic_store.lowerbound(id);
		u_it = user::tic_store.upperbound(id);
		for (t_it = l_it; t_it != u_it; t_it++)
		{
			if (*t_it == t_tmp)
			{
				ticket cmp = *t_it;
				for (; cmp.seat[seat].ticket_kind != kind; seat++);
				if (seat < cmp.num && cmp.seat[seat].left >= num)
				{
					(*t_it).seat[seat].left -= num;
					//修改一下map里的t_it
					flag = true;
					break;
				}
			}
		}
		if (!flag)//票数不够，没找到种类等情况
		{
			std::cout << 0 << '\n';
			return;
		}
		user::u_store.insert(id, u_tmp);
		train tr_tmp = train::tr_store[tr_id];
		int start = tr_tmp.pass_start, end = tr_tmp.pass_end;
		while (train::passes[start].name != loc1)start++;
		while (train::passes[end].name != loc2)end--;
		//火车上加票数
		station tmp;
		for (int i = start; i <= end; i++)
		{
			tmp = train::passes[i];
			tmp.sale[seat][date] += num;
			train::passes.edit(i, tmp);
		}
		std::cout << 1 << '\n';
		train::tr_store.insert(tr_id, tr_tmp);
		return;
	}
	string* query_ticket(string loc1, string loc2, int& num)
	{
		string* ans = new string[100];
		num = 0;
		map<1, string, string>::iterator s1, e1, s2, e2, i, j;
		s1 = train::world_map.lowerbound(loc1);
		e1 = train::world_map.upperbound(loc1);
		s2 = train::world_map.lowerbound(loc2);
		e2 = train::world_map.upperbound(loc2);
		for(i = s1; i != e1; i++)
			for (j = s2; j != e2; j++)
			{
				if (*i == *j)
				{
					ans[num] = *i;
					num++;
				}
			}
		return ans;
	}
	void query_ticket()
	{
		//我找到train_id, 只能说明火车经过这两点，还要查看有没有票
	}
	void query_transfer()
	{

	}
	void clean()
	{
		user::u_store.clear();
		user::tic_store.clear();
		train::tr_store.clear();
		train::world_map.clear();
		std::cout << 1 << '\n';
	}
	void exit()
	{
		std::cout << "BYE" << '\n';
		//exit();
	}
}
